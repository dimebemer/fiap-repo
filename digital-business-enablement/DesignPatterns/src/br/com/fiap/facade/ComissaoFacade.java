package br.com.fiap.facade;

import java.math.BigDecimal;

import br.com.fiap.dto.ColaboradorDTO;

public class ComissaoFacade {
	private BigDecimal comissaoGerente, comissaoVendedor, comissaoTotal;

	public ComissaoFacade(ColaboradorDTO gerente, ColaboradorDTO vendedor, BigDecimal valorVenda) {
		comissaoGerente = gerente.getCargo().calcula(valorVenda);
		comissaoVendedor = vendedor.getCargo().calcula(valorVenda);
		comissaoTotal = comissaoGerente.add(comissaoVendedor);
	}

	public BigDecimal getComissaoGerente() {
		return comissaoGerente;
	}

	public BigDecimal getComissaoVendedor() {
		return comissaoVendedor;
	}

	public BigDecimal getComissaoTotal() {
		return comissaoTotal;
	}

}
