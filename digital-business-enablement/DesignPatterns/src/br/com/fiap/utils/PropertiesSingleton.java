package br.com.fiap.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertiesSingleton {
	
	private static Properties properties;
	private static final String ARQ = "/conf.properties";
	
	private PropertiesSingleton() {}
	
	public static Properties getInstance(){
		if(properties == null) {
			properties = new Properties();
			
			try {
				properties.load(PropertiesSingleton.class.getResourceAsStream(ARQ));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return properties;
	}
}
