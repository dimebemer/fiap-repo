package br.com.fiap.enumeration;

import java.math.BigDecimal;

import br.com.fiap.strategy.ComissaoStrategy;

public enum Cargo implements ComissaoStrategy {
	GERENTE {
		@Override
		public BigDecimal calcula(BigDecimal valorTotal) {
			return null;
		}
	}, 
	VENDEDOR_A {
		@Override
		public BigDecimal calcula(BigDecimal valorTotal) {
			return null;
		}
	}, 
	VENDEDOR_B {
		@Override
		public BigDecimal calcula(BigDecimal valorTotal) {
			return null;
		}
	}
}
