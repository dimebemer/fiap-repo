package br.com.fiap.bo;

import java.math.BigDecimal;

import br.com.fiap.dto.ColaboradorDTO;

public class ColaboradorBO {
	private static final BigDecimal COMISSAO_GERENTE = new BigDecimal("0.02");
	private static final BigDecimal COMISSAO_VENDEDOR_A = new BigDecimal("0.01");
	private static final BigDecimal COMISSAO_VENDEDOR_B = new BigDecimal("0.005");

	public BigDecimal calculaComissao(ColaboradorDTO colaborador, BigDecimal valor) {
		switch (colaborador.getCargo()) {
			case GERENTE:
				return valor.multiply(COMISSAO_GERENTE);
			case VENDEDOR_A:
				return valor.multiply(COMISSAO_VENDEDOR_A);
			case VENDEDOR_B:
				return valor.multiply(COMISSAO_VENDEDOR_B);
		}

		return null;
	}
	
}
