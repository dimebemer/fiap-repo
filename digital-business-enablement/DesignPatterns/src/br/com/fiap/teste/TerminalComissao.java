package br.com.fiap.teste;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import br.com.fiap.bo.ColaboradorBO;
import br.com.fiap.dto.ColaboradorDTO;
import br.com.fiap.enumeration.Cargo;

public class TerminalComissao {
	
	private static DecimalFormat df = new DecimalFormat("R$ #,###.00");
	private static ColaboradorBO bo = new ColaboradorBO();
	
	public static void main(String[] args) {
		ColaboradorDTO gerente = new ColaboradorDTO();
		gerente.setId(1L);
		gerente.setCargo(Cargo.GERENTE);
		gerente.setNome("Diego Bicha");
		
		String comissaoGerente = df.format(bo.calculaComissao(gerente, new BigDecimal("10000")));
		System.out.println("Gerente: " + comissaoGerente);
		
		ColaboradorDTO vendedorA = new ColaboradorDTO();
		vendedorA.setId(1L);
		vendedorA.setCargo(Cargo.VENDEDOR_A);
		vendedorA.setNome("Diego Bicha");
		
		String comissaoVendA = df.format(bo.calculaComissao(vendedorA, new BigDecimal("5000")));
		System.out.println("Vendedor A: " + comissaoVendA);
		
		ColaboradorDTO vendedorB = new ColaboradorDTO();
		vendedorB.setId(1L);
		vendedorB.setCargo(Cargo.VENDEDOR_B);
		vendedorB.setNome("Diego Bicha");
		
		String comissaoVendB = df.format(bo.calculaComissao(vendedorB, new BigDecimal("1000")));
		System.out.println("Vendedor B: " + comissaoVendB);
	}
}
