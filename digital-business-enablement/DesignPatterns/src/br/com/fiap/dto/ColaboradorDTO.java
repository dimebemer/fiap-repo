package br.com.fiap.dto;

import java.io.Serializable;

import br.com.fiap.enumeration.Cargo;

public class ColaboradorDTO implements Serializable {
	private static final long serialVersionUID = -547382861021440942L;

	private Long id;
	private String nome;
	private Cargo cargo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}
