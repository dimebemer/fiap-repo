package br.com.fiap.strategy;

import java.math.BigDecimal;

public interface ComissaoStrategy {
	BigDecimal calcula(BigDecimal valorTotal);
}
